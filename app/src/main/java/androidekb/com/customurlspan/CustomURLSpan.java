package androidekb.com.customurlspan;

import android.content.Context;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.text.util.Linkify;
import android.view.View;
import android.widget.TextView;

public class CustomURLSpan extends android.text.style.URLSpan {
    private Command mClickAction;
    private Context mContext;


    public CustomURLSpan(String url, Command clickAction, Context context) {
        super(url);
        mClickAction = clickAction;
        mContext = context;
    }


    @Override
    public void onClick(View widget) {
        try {
            mClickAction.execute(getURL());
        } catch (Exception e) {
        }
    }

    public static void clickifyTextView(TextView tv, Command clickAction, Context context) {
        SpannableString current = new SpannableString(tv.getText());
        Linkify.addLinks(current, Linkify.ALL);
        URLSpan[] spans =
                current.getSpans(0, current.length(), URLSpan.class);

        for (URLSpan span : spans) {
            int start = current.getSpanStart(span);
            int end = current.getSpanEnd(span);

            current.removeSpan(span);
            current.setSpan(new CustomURLSpan(span.getURL(), clickAction, context)
                    , start, end, 0);

        }
        tv.setMovementMethod(LinkMovementMethod.getInstance());
        tv.setText(current, TextView.BufferType.SPANNABLE);
    }

    public interface Command {
        void execute(String url);
    }

/*
*This disable underline links
    @Override
    public void updateDrawState(TextPaint ds) {
        super.updateDrawState(ds);
        ds.setUnderlineText(false);
    }
*/

}
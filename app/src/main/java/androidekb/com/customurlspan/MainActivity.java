package androidekb.com.customurlspan;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        showDialog(this, "sdfsdf https://www.google.com sdfsdf http://mail.ru");
    }


    void showDialog(Context context, String message) {

        TextView text = new TextView(context);
        text.setText(message);

        // Linkify.addLinks(text, Linkify.EMAIL_ADDRESSES);
        CustomURLSpan.clickifyTextView(text,

                new CustomURLSpan.Command() {

                    @Override
                    public void execute(String url) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        startActivity(browserIntent);
                    }
                }


                ,context);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(text);
        alertDialogBuilder.setNegativeButton("Close", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alertDialogBuilder.create().show();
    }
}
